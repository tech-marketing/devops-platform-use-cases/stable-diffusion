import keras_cv
import streamlit as st
from tensorflow import keras
import matplotlib.pyplot as plt


title_1, title_2 = st.columns([2, 10])

with title_1:
        st.text(" ")
        st.image("gitlab-logo-500.png", width=100)

with title_2:
        st.title("    Cloud Native Review App - GitLab GPU Enabled Runners")

st.write("Enter a prompt")

query = st.text_input(label="", value="Cat tourist in Singapore")

model = keras_cv.models.StableDiffusion(img_width=512, img_height=512)

images = model.text_to_image(query, batch_size=1)

st.image(images)