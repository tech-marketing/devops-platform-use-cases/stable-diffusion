FROM python:3.9-slim-bullseye
LABEL maintainer="William Arias"
COPY .  /app/ 
WORKDIR /app
RUN pip install -r requirements.txt 
ENTRYPOINT [ "python" ]
CMD [ "server.py" ]

#COPY .    /app/
#WORKDIR /app
#RUN pip3 install -r requirements.txt
#EXPOSE 8501
#ENTRYPOINT ["streamlit", "run"]
#CMD ["ui.py"]
#CMD [ "python", "./main.py"]

